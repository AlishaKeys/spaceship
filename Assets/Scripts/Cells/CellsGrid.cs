﻿using System.Collections.Generic;

using UnityEngine;
using System.Linq;
using Command.Spaceship;
using TMPro;

namespace Cells
{
    /// <summary>
    /// Класс для управления и генерации клеток
    /// </summary>
    public class CellsGrid : MonoBehaviour
    {
        #region Serialize fields
        [SerializeField, Tooltip("Префаб клетки")]
        private GameObject cellPrefab;
        [SerializeField, Tooltip("Варианты спрайтов для клетки")]
        private Sprite[] randomCellSprites;
        [SerializeField, Tooltip("Префаб планеты")]
        private GameObject planetPrefab;
        [SerializeField, Tooltip("Варианты спрайтов для планетки")]
        private Sprite[] randomPlanetSprites;
        [SerializeField, Tooltip("Количество столбцов")]
        private byte columns = 5;
        [SerializeField, Tooltip("Колиство строк")]
        private byte rows = 5;

        [SerializeField, Tooltip("Долго расшифровывать")]
        #endregion

        #region Private fields
        private float cellPPU = 100f;
        private const float cellPixelSize = 128f;

        private float pixelSize { get { return cellPixelSize / cellPPU; } }

        private List<GridCoordinates> coordinates = new List<GridCoordinates>();
        private int room = 0;

        private int planetCount { get { return (int)(rows * columns * planetPercent); } }
        private Dictionary<GameObject, int> planets = new Dictionary<GameObject, int>();

        private Spaceship spaceship;

        private const float planetPercent = 0.3f;
        private const float fault = .1f;
        #endregion

        #region MonoBehaviour call
        private void Awake()
        {
            spaceship = FindObjectOfType<Spaceship>();
            spaceship.MoveDirectionEvent += GetDirection;
            CreateCellObjects(Vector2.zero);
        }
        #endregion

        #region Cells generation
        /// <summary>
        /// Создаем здесь клеточки и планетки в них
        /// </summary>
        /// <param name="direction">Дирекшен!</param>
        private void CreateCellObjects(Vector2 direction)
        {
            var firstPos = Vector2.zero;
            var lastPos = Vector2.zero;

            for (byte row = 0; row < rows; row++)
            {
                for (byte column = 0; column < columns; column++)
                {
                    var position = GetPositionForCoordinates(column, row, direction);

                    var cell = Instantiate(cellPrefab, new Vector2(position.x, position.y), Quaternion.identity, transform);

                    cell.GetComponent<SpriteRenderer>().sprite = randomCellSprites[Random.Range(0, randomCellSprites.Length)];
                    CreatePlanet(position, column * row);

                    firstPos = row == 0 && column == 0 ? position : firstPos;
                    lastPos = row == rows - 1 && column == columns - 1 ? position : lastPos;
                }
            }
            coordinates.Add(new GridCoordinates(firstPos, lastPos));
            SetRaiting();
            planets.Clear();
        }

        /// <summary>
        /// Получаем здесь нормальные координатки
        /// </summary>
        /// <param name="column">Колонка</param>
        /// <param name="row">Строка</param>
        /// <param name="direction">Дирекшен</param>
        /// <returns></returns>
        private Vector2 GetPositionForCoordinates(byte column, byte row, Vector2 direction)
        {
            var worldPosition = Vector2.zero;

            worldPosition.x = coordinates.Count == 0 ?
                (-pixelSize * ((columns - 1) / 2f)) :
                (direction.x * pixelSize * columns + coordinates[room].firstPosition.x);
            worldPosition.y = coordinates.Count == 0 ?
                (-pixelSize * ((rows - 1) / 2f)) :
                (direction.y * pixelSize * rows + coordinates[room].firstPosition.y);
            worldPosition.x += pixelSize * column;
            worldPosition.y += pixelSize * row;
            return worldPosition;
        }

        /// <summary>
        /// Получаем от плеера дирекшен и всякие штуки-дрюки делаем, чтобы быть в курсе: создавать нам новые клетки или нет
        /// </summary>
        /// <param name="direction">Дирекшен -_- </param>
        private void GetDirection(Vector2 direction)
        {
            Vector2 spaceshipPos = spaceship.transform.position;

            for (int i = 0; i < coordinates.Count; i++)
            {
                float distanceX = (spaceshipPos - coordinates[i].lastPosition).sqrMagnitude;
                float distanceY = (spaceshipPos - coordinates[i].firstPosition).sqrMagnitude;

                float box = (coordinates[i].firstPosition - coordinates[i].lastPosition).sqrMagnitude;

                var distBetweenLastPosX = spaceshipPos.x - fault <= coordinates[i].lastPosition.x && spaceshipPos.y - fault <= coordinates[i].lastPosition.y;
                var distBetweenLastPosY = spaceshipPos.x + fault >= coordinates[i].firstPosition.x && spaceshipPos.y + fault >= coordinates[i].firstPosition.y;

                if (distBetweenLastPosX && distBetweenLastPosY)
                {
                    room = i;
                    return;
                }

                Camera.main.transform.position = new Vector3(spaceshipPos.x, spaceshipPos.y, -10);
            }
            CreateCellObjects(direction);
        }
        #endregion

        #region Planet control
        /// <summary>
        /// Создаем планеты
        /// </summary>
        /// <param name="position">Позицию получаем</param>
        /// <param name="cellsCount">Считаем сколько у нас клеток осталось</param>
        private void CreatePlanet(Vector2 position, int cellsCount)
        {
            if (planetCount > planets.Count)
            {
                int randomSpawn = Random.Range(0, 2);
                if (randomSpawn == 1 || rows * columns - cellsCount <= planetCount)
                {
                    var randomSprite = Random.Range(0, randomPlanetSprites.Length);
                    var randomRaiting = Random.Range(0, 1000);
                    var planet = Instantiate(planetPrefab, new Vector2(position.x, position.y), Quaternion.identity, transform);
                    planet.GetComponent<SpriteRenderer>().sprite = randomPlanetSprites[randomSprite];
                    planets.Add(planet, randomRaiting);
                }
            }
        }

        /// <summary>
        /// Устанавливаем рейтинг
        /// </summary>
        private void SetRaiting()
        {
            var maxRaiting = planets.Max(x => x.Value);
            var keyForValue = planets.FirstOrDefault(x => x.Value == maxRaiting).Key;

            keyForValue.GetComponentInChildren<TextMeshPro>().text = maxRaiting.ToString();
        }
    }
    #endregion
}
