﻿using System;
using UnityEngine;

namespace Cells
{
    /// <summary>
    /// Здесь храним инфу о наших первых и последних позициях клетки
    /// </summary>
    [Serializable]
    public struct GridCoordinates
    {
        public Vector2 firstPosition;
        public Vector2 lastPosition;

        public GridCoordinates(Vector2 firstPosition, Vector2 lastPosition)
        {
            this.firstPosition = firstPosition;
            this.lastPosition = lastPosition;
        }

    }
}