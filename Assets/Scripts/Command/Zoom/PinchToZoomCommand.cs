﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.PinchToZoom
{
    /// <summary>
    /// Комманда зума
    /// </summary>
    public class PinchToZoomCommand : InputCommand<PinchToZoom>
    {
        public PinchToZoomCommand(ExecuteCallback executeMethod, string name) : base(executeMethod, name) { }
    }
}