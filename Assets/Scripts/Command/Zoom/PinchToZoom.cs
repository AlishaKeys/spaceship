﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.PinchToZoom
{
    /// <summary>
    /// Класс движения зума для камеры
    /// </summary>
    public class PinchToZoom : MonoBehaviour
    {
        public Camera Camera;
        public float ZoomSpeed = 2f;

        private const int ZoomOutMin = 5;
        private const int ZoomOutMax = 1000;

        public void Move(float positionOffset)
        {
            Camera.main.orthographicSize = Mathf.Clamp(Camera.orthographicSize - positionOffset, ZoomOutMin, ZoomOutMax);
        }
    }
}
