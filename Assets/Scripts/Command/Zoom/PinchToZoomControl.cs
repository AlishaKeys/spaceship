﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.PinchToZoom
{
    /// <summary>
    /// Контроль за зум камеры
    /// </summary>
    public class PinchToZoomControl : MonoBehaviour
    {
        public const float CommandPauseTime = .01f;

        [SerializeField, Tooltip("Камеры со скриптом зума")]
        private PinchToZoom pinchToZoom = null;

        private List<PinchToZoomCommand> pinchToZoomCommands = new List<PinchToZoomCommand>();
        private Coroutine executeRoutine;

        private void Update()
        {
            CheckForZoomCommands();
        }

         /// <summary>
         /// Чекаем пальцы
         /// </summary>
        private void CheckForZoomCommands()
        {
            var zoomCommand = PinchToZoomInputHandler.HandleInput();
            if (zoomCommand != null && executeRoutine == null)
            {
                AddToCommands(zoomCommand);
            }
            ExecuteCommands();
        }

        /// <summary>
        /// Добавляем комманду
        /// </summary>
        /// <param name="PinchToZoomCommand"></param>
        private void AddToCommands(PinchToZoomCommand PinchToZoomCommand)
        {
            pinchToZoomCommands.Add(PinchToZoomCommand);
        }

        /// <summary>
        /// Выполняем комманду
        /// </summary>
        private void ExecuteCommands()
        {
            if (executeRoutine != null)
            {
                return;
            }

            executeRoutine = StartCoroutine(ExecuteCommandsRoutine());
        }

        /// <summary>
        /// Выполняем комманду
        /// </summary>
        /// <returns></returns>
        private IEnumerator ExecuteCommandsRoutine()
        {
            for (int i = 0, count = pinchToZoomCommands.Count; i < count; i++)
            {
                var command = pinchToZoomCommands[i];
                command.Execute(pinchToZoom);
                yield return new WaitForSeconds(CommandPauseTime);
            }

            executeRoutine = null;
        }
    }
}
