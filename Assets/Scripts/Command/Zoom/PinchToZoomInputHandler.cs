﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.PinchToZoom
{
    public class PinchToZoomInputHandler
    {
        private static readonly PinchToZoomCommand Zoom =
            new PinchToZoomCommand(delegate (PinchToZoom PinchToZoom) { PinchToZoom.Move(deltaMagnitudeDiff); }, "zoom");

        private static float deltaMagnitudeDiff;
        private const int minTouchCount = 2;

        public static PinchToZoomCommand HandleInput()
        {
            if (Input.touchCount == minTouchCount)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                return Zoom;
            }
            return null;
        }
    }
}
