﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.Spaceship
{
    /// <summary>
    /// Класс управления свайпами
    /// </summary>
    public class SpaceshipInputHandler
    {
        #region Private fields
        private static readonly SpaceshipCommand MoveUp =
            new SpaceshipCommand(delegate (Spaceship Spaceship) { Spaceship.Move(CardinalDirection.Up); }, "moveUp");

        private static readonly SpaceshipCommand MoveDown =
            new SpaceshipCommand(delegate (Spaceship Spaceship) { Spaceship.Move(CardinalDirection.Down); }, "moveDown");

        private static readonly SpaceshipCommand MoveLeft =
                    new SpaceshipCommand(delegate (Spaceship Spaceship) { Spaceship.Move(CardinalDirection.Left); }, "moveLeft");

        private static readonly SpaceshipCommand MoveRight =
            new SpaceshipCommand(delegate (Spaceship Spaceship) { Spaceship.Move(CardinalDirection.Right); }, "moveRight");

        private static float dragDistance;  
        private static int swipeID;
        private static Vector2 startPos;
        private const int minTouchCount = 1;
        #endregion

        public static SpaceshipCommand HandleInput()
        {
            if(minTouchCount == Input.touchCount)
            {
                var touch = Input.GetTouch(0);
                var touchPos = touch.position;
                var tapCount = touch.tapCount;
                if (touch.phase == TouchPhase.Began && swipeID == -1)
                {
                    swipeID = touch.fingerId;
                    startPos = touchPos;
                }
                else if (touch.fingerId == swipeID)
                {
                    var delta = touchPos - startPos;
                    if (touch.phase == TouchPhase.Moved && delta.magnitude > dragDistance)
                    {
                        swipeID = -1;
                        if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                        {
                            if (delta.x > 0)
                                return MoveRight;
                            else
                                return MoveLeft;
                        }
                        else
                        {
                            if (delta.y > 0)
                                return MoveUp;
                            else
                                return MoveDown;
                        }
                    }
                }
            }
            return null;
        }
    }
}
