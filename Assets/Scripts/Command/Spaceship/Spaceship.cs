﻿using Cells;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CardinalDirection
{
    Up,
    Down,
    Right,
    Left
}

namespace Command.Spaceship
{
    /// <summary>
    /// Движение корабля
    /// </summary>
    public class Spaceship : MonoBehaviour
    {
        #region Public fields
        public float MoveOffset = 1.3f;
        public delegate void MoveDirection(Vector2 dir);
        public event MoveDirection MoveDirectionEvent;
        #endregion

        #region Private fields
        private const float moveTime = 0f;

        private Coroutine moveRoutine;
        #endregion

        #region Move
        private void Start()
        {
            transform.position = Vector2.zero;
        }

        /// <summary>
        /// Направление корабля
        /// </summary>
        /// <param name="direction">Направление</param>
        public void Move(CardinalDirection direction)
        {
            if (moveRoutine != null)
                return;

            Vector2 directionVector = Vector2.zero;
            switch (direction)
            {
                case CardinalDirection.Up:
                    directionVector = Vector2.up;
                    break;
                case CardinalDirection.Down:
                    directionVector = Vector2.down;
                    break;
                case CardinalDirection.Right:
                    directionVector = Vector2.right;
                    break;
                case CardinalDirection.Left:
                    directionVector = Vector2.left;
                    break;
            }

            moveRoutine = StartCoroutine(MoveRoutine(directionVector * MoveOffset));
            MoveDirectionEvent(directionVector);
        }

        /// <summary>
        /// Корутина для движения корабля, если бы я вдруг захотела это сделать покрасивей
        /// </summary>
        /// <param name="positionOffset">Сдвиг по клеткам</param>
        /// <returns></returns>
        private IEnumerator MoveRoutine(Vector3 positionOffset)
        {
            var startPos = transform.position;
            var targetPos = transform.position + positionOffset;
            for (float t = 0; t < moveTime; t += Time.deltaTime)
            {
                transform.position = Vector3.Lerp(startPos, targetPos, t / moveTime);
                yield return null;
            }

            transform.position = targetPos;
            moveRoutine = null;
        }
        #endregion
    }
}
