﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.Spaceship
{
    /// <summary>
    /// Управление командой корабля
    /// </summary>
    public class SwipeControl : MonoBehaviour
    {
        public const float CommandPauseTime = 0.01f;

        [SerializeField, Tooltip("Кораблик")]
        private Spaceship spaceship = null;

        private List<SpaceshipCommand> SpaceshipCommands = new List<SpaceshipCommand>();
        private Coroutine executeRoutine;

        private void Update()
        {
            CheckForSpaceshipCommands();
        }

        /// <summary>
        /// Чекаем всё, что мы свайпаем
        /// </summary>
        private void CheckForSpaceshipCommands()
        {
            var SpaceshipCommand = SpaceshipInputHandler.HandleInput();
            if (SpaceshipCommand != null && executeRoutine == null)
            {
                AddToCommands(SpaceshipCommand);
            }
            ExecuteCommands();
        }

        /// <summary>
        /// Добавляем в комманду
        /// </summary>
        /// <param name="SpaceshipCommand"></param>
        private void AddToCommands(SpaceshipCommand SpaceshipCommand)
        {
            SpaceshipCommands.Add(SpaceshipCommand);
        }

        /// <summary>
        /// Выполняем комманду
        /// </summary>
        private void ExecuteCommands()
        {
            if (executeRoutine != null)
            {
                return;
            }

            executeRoutine = StartCoroutine(ExecuteCommandsRoutine());
        }

        /// <summary>
        /// Выполняем комманду 
        /// </summary>
        /// <returns></returns>
        private IEnumerator ExecuteCommandsRoutine()
        {
            for (int i = 0, count = SpaceshipCommands.Count; i < count; i++)
            {
                var command = SpaceshipCommands[i];
                command.Execute(spaceship);
                yield return new WaitForSeconds(CommandPauseTime);
            }

            SpaceshipCommands.Clear();

            executeRoutine = null;
        }
    }
}
