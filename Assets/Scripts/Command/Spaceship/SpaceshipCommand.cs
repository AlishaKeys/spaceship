﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command.Spaceship
{
    public class SpaceshipCommand : InputCommand<Spaceship>
    {
        public SpaceshipCommand(ExecuteCallback executeMethod, string name) : base(executeMethod, name) { }
    }
}
