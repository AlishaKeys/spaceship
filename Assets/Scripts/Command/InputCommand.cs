﻿namespace Command
{
    /// <summary>
    /// Комманда для инпута
    /// </summary>
    /// <typeparam name="ObjCommand">Объект комманды</typeparam>
    public class InputCommand<ObjCommand>
    {
        private readonly string commandName;

        public InputCommand(ExecuteCallback executeMethod, string name)
        {
            Execute = executeMethod;
            commandName = name;
        }

        public delegate void ExecuteCallback(ObjCommand spaceship);

        public ExecuteCallback Execute { get; private set; }

        public override string ToString()
        {
            return commandName;
        }
    }
}
